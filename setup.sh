#!/usr/bin/env bash
if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
        echo "Error: Please source this script"
        exit 1
fi

if [ -z "${BASH_VERSION:-}" ]; then
	echo "Error: Bash is required to run demi." >&2
    return 1
fi

kernel_name="$(uname -s)"

if [[ "${kernel_name}" == "Darwin" ]]; then
    command -v brew >/dev/null 2>&1 || {
        echo 'We recommend you install brew:'
        # shellcheck disable=SC2016
        echo '/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"'
    }
    echo 'macOS: make sure to have recent bash version and GNU coreutils installed.'
    #echo 'brew install bash coreutils'
    command -v realpath >/dev/null 2>&1 || {
        echo 'We recommend you install GNU coreutils:'
        echo 'brew install coreutils'
    }
fi

if [ ! -v DEMI_ROOT ]; then
    # shellcheck source=/dev/null
    tmp_path=$(realpath "${BASH_SOURCE}")
    DEMI_ROOT="${tmp_path%/*}"
    # DEMI_ROOT="$(realpath "${BASH_SOURCE%/*}")"
    export DEMI_ROOT
    echo "DEMI_ROOT: ${DEMI_ROOT}"
fi

export PATH="$DEMI_ROOT/bin:$PATH"
eval "$(demi init -)"

###### don't assume these dependencies now ######

# export PYENV_ROOT="$HOME/.pyenv"
# export PATH="$HOME/.pyenv/bin:$PATH"
# eval "$(pyenv init --path)"
# eval "$(pyenv init -)"

# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"                   # This loads nvm
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion
