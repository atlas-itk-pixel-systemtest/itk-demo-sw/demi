#!/bin/sh

AUTH_SERVER='auth.cern.ch'
AUTH_REALM='cern'
EXPECTED_ARGS=3

if [ $# -ne $EXPECTED_ARGS ]
then
echo "Usage: get_api_token.sh <client_id> <client_secret> <target_application>"
exit 1
fi

curl --location --request POST "https://$AUTH_SERVER/auth/realms/$AUTH_REALM/api-access/token" \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'grant_type=client_credentials' \
--data-urlencode "client_id=$1" \
--data-urlencode "client_secret=$2" \
--data-urlencode "audience=$3"
