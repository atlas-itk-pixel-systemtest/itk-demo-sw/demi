# demi

DeMi - Demonstrator Microservices management tool.

Extended information about the demi commands can be found in the [wiki](https://gitlab.cern.ch/groups/atlas-itk-pixel-systemtest/itk-demo-sw/-/wikis/demi).

Use a recent BASH shell.

```bash
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi.git $HOME/.demi
source $HOME/.demi/setup.sh
cd {{to-your-itk-demo-repo}}
demi
```
