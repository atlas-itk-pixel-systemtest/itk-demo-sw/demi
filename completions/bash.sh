#!/bin/bash
_demi() {
	COMPREPLY=()
	local word="${COMP_WORDS[COMP_CWORD]}"
	local COMMAND_DIR="$DEMI_ROOT/.configuration/commands"

	if [ "$COMP_CWORD" -eq 1 ]; then
		local comp_file="$COMMAND_DIR/demi"
		if [ -f "$comp_file" ]; then
			COMPREPLY=($(compgen -W "$(cat $comp_file)" -- "$word"))
		fi
	else
		local words=("${COMP_WORDS[@]}")
		unset "words[0]"
		unset "words[$COMP_CWORD]"
		local comp_file="$COMMAND_DIR/demi_${words[@]}"
		if [ -f "$comp_file" ]; then
			COMPREPLY=($(compgen -W "$(cat $comp_file)" -- "$word"))
		fi
	fi
}

complete -F _demi demi
