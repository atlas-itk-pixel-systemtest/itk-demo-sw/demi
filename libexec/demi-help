#!/usr/bin/env bash
# Summary: Display help for a command.
#
# Usage: demi help [--usage] COMMAND
#
# Parses and displays help contents from a command's source file.
#
# A command is considered documented if it starts with a comment block
# that has a `Summary:' or `Usage:' section. Usage instructions can
# span multiple lines as long as subsequent lines are indented.
# The remainder of the comment block is displayed as extended
# documentation.

# shellcheck source=/dev/null

[ "${DEMI_DEBUG-}" ] && set -x

# log debug "$0 $*"

# completions
if [ "${1-}" = "--complete" ]; then
	echo --usage
	exec demi-commands
fi

. "${DEMI_ROOT}"/utils/demi_tools

extract_initial_comment_block() {
	LC_ALL= \
		LC_CTYPE=C \
		sed -ne "
    /^#/ !{
      q
    }

    s/^#$/# /

    /^# / {
      s/^# //
      p
    }
  "
}

collect_documentation() {
	# `tail` prevents "broken pipe" errors due to `head` closing thge pipe without reading everything
	# https://superuser.com/questions/554855/how-can-i-fix-a-broken-pipe-error/642932#642932
	# shellcheck disable=SC2016
	$(type -P gawk awk | tail -n +1 | head -1) '
    /^Summary:/ {
      summary = substr($0, 10)
      next
    }

    /^Usage:/ {
      reading_usage = 1
      usage = usage "\n" $0
      next
    }

    /^( *$|       )/ && reading_usage {
      usage = usage "\n" $0
      next
    }

    {
      reading_usage = 0
      help = help "\n" $0
    }

    function escape(str) {
      gsub(/[`\\$"]/, "\\\\&", str)
      return str
    }

    function trim(str) {
      sub(/^\n*/, "", str)
      sub(/\n*$/, "", str)
      return str
    }

    END {
      if (usage || summary) {
        print "summary=\"" escape(summary) "\""
        print "usage=\"" escape(trim(usage)) "\""
        print "help=\"" escape(trim(help)) "\""
      }
    }
  '
}

documentation_for() {
	local filename
	filename="$(command_path "$1")"
	if [ -n "$filename" ]; then
		extract_initial_comment_block <"$filename" | collect_documentation
	fi
}

print_summary() {
	local command="$1"
	local summary usage help
	eval "$(documentation_for "$command")"

	if [ -n "$summary" ]; then
		# shellcheck disable=SC2154
		printf "   %b%-9s%b   %s\n" "${cyan}" "$command" "${end}" "$summary"
	fi
}

print_summaries() {
	for command; do
		print_summary "$command"
	done
}

print_help() {
	local command="$1"
	local summary usage help
	eval "$(documentation_for "$command")"
	[ -n "$help" ] || help="$summary"

	if [ -n "${usage:-}" ] || [ -n "$summary" ]; then
		print_usage "$1"
		if [ -n "$help" ]; then
			echo
			echo "$help"
			echo
		fi

		local filename
		filename="$(command_path "$1")"
		subcmds=$(doc_subcommands "$filename")
		# shellcheck disable=SC2154
		echo -e "${bold}Subcommands:${end}"
		echo
		# for subcmd in $subcmds; do
		doc_subcommands "$filename" | while read -r cmddoc; do
			IFS='#' read -ra doc_split <<<"$cmddoc"
			# doc_split=$(echo $cmddoc | tr "#")
			printf "   %b%-13s%b   %s\n" "${cyan}" "${doc_split[0]}" "${end}" "${doc_split[1]}"
			# echo -e "   ${cyan}${doc_split[0]}${end}${doc_split[1]}"
			# grep -B 1 "$subcmd" "$filename" | grep "command"
		done
	else
		log warn "Sorry, this command isn't documented yet."
		return 1
	fi
}

print_usage() {
	local command="$1"
	local summary usage help
	eval "$(documentation_for "$command")"
	# [ -z "$usage" ] || echo "$usage"
	if [ "$usage" ]; then
		echo "$usage"
	else
		# try automatic usage message from subcommands
		local filename
		filename="$(command_path "$1")"
		if [ -n "$filename" ]; then
			log debug "help: Usage for $filename"
			cmds="$(list_subcommands "$filename" | tr '\n' '|')"
			cmds="${cmds%|}"
			if [ -z "$cmds" ]; then
				log warn "Sorry, 'demi ${command}' doesn't have usage info yet."
				return
			fi
			echo -e "${bold}Usage: ${end}demi $command [$cmds]"
		fi
		return
	fi
}

unset usage
if [ "$1" = "--usage" ]; then
	usage="1"
	shift
fi

demi_help() {
	echo
	echo -e "${bold}usage: ${end}demi ${goptions} <command> [<args>]"
	[ -z "${usage:-}" ] || exit
	# echo
	# echo -e "${bold}Global Options:${end}"
	# print_goptions
	echo
	echo -e "${bold}Commands:"
	# shellcheck disable=SC2046
	print_summaries $(exec demi-commands --no-hidden --no-group | sort -u)

	echo
	echo -e "${bold}Backend commands:"
	# shellcheck disable=SC2046
	print_summaries $(exec demi-commands --group backend | sort -u)

	echo
	echo "See \`demi help <command>' for information on a specific command."
	echo "Homepage: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw"
	echo
}

log debug "help: $*"

if [ -z "$1" ] || [ "$1" == "demi" ]; then
	# help for demi itself
	demi_help
else
	command="$1"
	if [ -n "$(command_path "$command")" ]; then
		echo
		if [ -v usage ]; then
			print_usage "$command"
		else
			print_help "$command"
		fi
		echo
	else
		echo "No such command \`$command'" >&2
		exit 1
	fi
fi
