
FROM centos:centos7

SHELL ["/bin/bash", "--login", "-c"]
RUN cd && touch .bashrc

RUN yum install -y which epel-release git
RUN cd && git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi.git
RUN echo ". $HOME/demi/setup.sh" >> /root/.bashrc
RUN echo "cd $HOME" >> /root/.bashrc

# RUN yum group info "Development Tools"
# RUN yum install -y wget perl-CPAN gettext-devel perl-devel  openssl-devel zlib-devel curl-devel expat-devel getopt asciidoc xmlto docbook2X
RUN yum install -y gcc make autoconf
#RUN demi -d -v self update
RUN demi git install
RUN git --version

 